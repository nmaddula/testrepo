package Selenium.Selenium_April;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Sampledriver {

	public static void main(String[] args) throws Throwable {
		XSSFWorkbook TestcaseFile = new XSSFWorkbook("C:\\Users\\Navytha\\Desktop\\TC_1.xlsx");
		XSSFSheet testcaseslist = TestcaseFile.getSheetAt(0);
		int totalnoofcolumnsinheaderRow = testcaseslist.getRow(0).getLastCellNum();
		int TestDescriptioncolumnNo = -1, TestActionColumnNo = -1, TestDataColumnNo = -1, LocatorcolumnNo = -1;
		
		for (int i = 0; i < totalnoofcolumnsinheaderRow; i++) {
			XSSFCell HeaderCell = testcaseslist.getRow(0).getCell(i);
			if (HeaderCell != null && HeaderCell.toString().trim().isEmpty()) {
				String Headervalue = HeaderCell.toString().trim();
				if (Headervalue.equalsIgnoreCase("TestDescription")) {
					TestDescriptioncolumnNo = i;
				} else if (Headervalue.equalsIgnoreCase("TestAction")) {
					TestActionColumnNo = i;
				} else if (Headervalue.equalsIgnoreCase("TestData")) {
					TestDataColumnNo = i;
				} else if (Headervalue.equalsIgnoreCase("Locator")) {
					LocatorcolumnNo = i;
				}
			}

		}
		if (TestDescriptioncolumnNo < 0) {
			throw new Exception("Test description is missing");
		}
		if (TestActionColumnNo < 0) {
			throw new Exception("Test Action is missing");
		}
		if (TestDataColumnNo < 0) {
			throw new Exception("Test Data is missing");
		}
		if (LocatorcolumnNo < 0) {
			throw new Exception("Locator is missing");
		}

		int totalrows = testcaseslist.getLastRowNum() + 1;
		for (int i = 0; i < totalrows; i++) {
			String TestDescriptionValue = null, TestActionValue = null, TestDataValue =null,LocatorValue =null;
			XSSFCell TestDescriptioncell = testcaseslist.getRow(i).getCell(TestDescriptioncolumnNo);
			if (TestDescriptioncell != null && !TestDescriptioncell.toString().trim().isEmpty()) {
				 TestDescriptionValue = TestDescriptioncell.toString().trim();
			}
			XSSFCell TestActioncell = testcaseslist.getRow(i).getCell(TestActionColumnNo);
			if (TestActioncell != null && !TestActioncell.toString().trim().isEmpty()) {
				 TestActionValue = TestActioncell.toString().trim();
			}
			XSSFCell TestDatacell = testcaseslist.getRow(i).getCell(TestDataColumnNo);
			if (TestDatacell != null && !TestDatacell.toString().trim().isEmpty()) {
				 TestDataValue = TestDatacell.toString().trim();
			}
			XSSFCell Locatorcell = testcaseslist.getRow(i).getCell(LocatorcolumnNo);
			if (Locatorcell != null && !Locatorcell.toString().trim().isEmpty()) {
				 LocatorValue = Locatorcell.toString().trim();
			}
			System.out.println("********************************************************************************");
			System.out.println(TestDescriptionValue);
			

		}
		TestcaseFile.close();
	}

}
